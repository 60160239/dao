/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import libraryconnect.LibraryConnect;
/**
 *
 * @author informatics
 */
public class database {
    static String url = "jdbc:sqlite:./db/library.db";
    static Connection conn = null;
    
   public static void closeDatabase() {
        try {
            if(conn!=null)
                conn.close();
            System.out.println("Close Database");
            conn=null;
        } catch (SQLException ex) {
            Logger.getLogger(LibraryConnect.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void showInfo() {
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT userId,\n" +
                    "       loginName,\n" +
                    "       password,\n" +
                    "       name,\n" +
                    "       surname,\n" +
                    "       typeId\n" +
                    "  FROM user";
            ResultSet rs = stm.executeQuery(sql);
            while(rs.next()){
                System.out.println(rs.getInt("userId")+" "+rs.getString("loginName")+" ");
            }
        } catch (SQLException ex) {
            Logger.getLogger(LibraryConnect.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static Connection connectDatabase() {
        if(conn!=null)return conn;
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connected to Database");
            return conn;
        } catch (SQLException ex) {
            Logger.getLogger(LibraryConnect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
