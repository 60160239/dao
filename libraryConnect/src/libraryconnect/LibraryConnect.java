/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libraryconnect;

import database.User;
import database.UserDao;
import database.database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author informatics
 */
public class LibraryConnect {
    static String url = "jdbc:sqlite:./db/library.db";
    static Connection conn = null;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        
/*        User newuser = new User();
        newuser.setUserId(-1);
        newuser.setLoginName("ABC");
        newuser.setPassword("password");
        newuser.setName("ABC");
        newuser.setSurname("DEF");
        newuser.setTypeId(1);
        UserDao.insert(newuser);
        
        ArrayList<User> list = UserDao.getUsers();
        for(User user:list){
            System.out.println(user);

        }*/

/*        User edituser = UserDao.getUser(1);
        System.out.println(edituser);
        edituser.setName("Konamimet");
        UserDao.update(edituser);
        User user1 = UserDao.getUser(1);
        System.out.println(user1);
*/
        User deleteuser = UserDao.getUser(3);
        System.out.println(deleteuser);
        UserDao.delete(deleteuser);
        System.out.println(UserDao.getUser(3));
    }


    private static void showInfo() {
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT userId,\n" +
                    "       loginName,\n" +
                    "       password,\n" +
                    "       name,\n" +
                    "       surname,\n" +
                    "       typeId\n" +
                    "  FROM user";
            ResultSet rs = stm.executeQuery(sql);
            while(rs.next()){
                System.out.println(rs.getInt("userId")+" "+rs.getString("loginName")+" ");
            }
        } catch (SQLException ex) {
            Logger.getLogger(LibraryConnect.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
}
